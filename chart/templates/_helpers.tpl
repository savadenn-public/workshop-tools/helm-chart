{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "workshop.chart" -}}
{{- printf "%s-%s" $.Chart.Name $.Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Common labels
*/}}
{{- define "workshop.labels" -}}
{{- $ := index . 0 -}}
{{- with index . 1 -}}
helm.sh/chart: {{ include "workshop.chart" $ }}
app.kubernetes.io/name: {{ .name }}
app.kubernetes.io/instance: {{ $.Release.Name }}
{{- if .version }}
app.kubernetes.io/version: {{ .version }}
{{- end }}
app.kubernetes.io/managed-by: {{ $.Release.Service }}
{{- end }}
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "workshop.varSecretName" -}}
{{- $ := index . 0 }}
{{- with index . 1 }}
{{- printf "%s-%s" .name "vars" | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}

{{/*
Extract paths or resolve to /
*/}}
{{- define "workshop.defaultPath" -}}
{{- if . }}{{ first . }}{{- else }}{{ "/" }}{{- end }}
{{- end }}

{{/*
Find secret value in another
*/}}
{{- define "workshop.findOrGenerate" -}}
{{- $existingSecretData := index . 0 }}
{{- $key := index . 1 }}
{{- if and $existingSecretData (hasKey $existingSecretData $key) }}
{{- get $existingSecretData $key }}
{{- else }}
{{- randAlphaNum 50 | b64enc | quote }}
{{- end }}
{{- end }}
