---
{{- with .Values.services.auth }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: auth
  labels:
    {{- include "workshop.labels" (list $ .) | nindent 4 }}
spec:
  replicas: {{ .replicas }}
  selector:
    matchLabels:
      app.kubernetes.io/name: "auth"
      app.kubernetes.io/type: "server"
      app.kubernetes.io/instance: {{ $.Release.Name }}
  template:
    metadata:
      labels:
        app.kubernetes.io/name: "auth"
        app.kubernetes.io/type: "server"
        app.kubernetes.io/instance: {{ $.Release.Name }}
        {{- if eq (default "" .version) "latest"}}
        # In case of latest branch, it must recreate pod to download new image
        # Writing date trigger change in configuration, so a redeployment
        specific.renew: {{ now | date "2006-01-02T15_04_05Z" }}
        {{- end }}
    spec:
      containers:
        - name: "php"
          # project: "https://gitlab.com/savadenn-public/workshop-tools/authentication"
          image: "registry.gitlab.com/savadenn-public/workshop-tools/authentication/php:{{ .version }}"
          imagePullPolicy:  {{ ternary "Always" "IfNotPresent" ( eq (default "" .version) "latest") }}
          envFrom:
            - secretRef:
                name: auth
          env:
            - name: APP_ENV
              value: prod
            - name: APP_RELEASE
              value: "{{ .version }}"
            - name: POSTGRES_PASS
              valueFrom:
                secretKeyRef:
                  key: POSTGRES_DB_AUTH
                  name: database
            - name: SMTP_PASSWORD
              valueFrom:
                secretKeyRef:
                  key: SMTP_PASSWORD
                  name: smtp
            - name: MERCURE_JWT_SECRET
              valueFrom:
                secretKeyRef:
                  key: "MERCURE_SUBSCRIBER_JWT_KEY"
                  name: mercure
          volumeMounts:
            - name: config-volume
              mountPath: /srv/api/.env
              subPath: .env
            - mountPath: /var/run/php
              name: php-socket
            - name: jwt
              mountPath: /srv/api/config/jwt/
          lifecycle:
            preStop:
              exec:
                command: ["/bin/sh", "-c", "/bin/sleep 1; kill -QUIT 1"]
          readinessProbe:
            exec:
              command:
                - docker-healthcheck
            initialDelaySeconds: 10
            periodSeconds: 3
          livenessProbe:
            exec:
              command:
                - docker-healthcheck
            initialDelaySeconds: 10
            periodSeconds: 3
          resources:
            {{- toYaml .resources | nindent 12 }}

        - name: "caddy"
          # project: "https://gitlab.com/savadenn-public/workshop-tools/authentication"
          image: "registry.gitlab.com/savadenn-public/workshop-tools/authentication/{{ .branch }}web:{{ .version }}"
          imagePullPolicy:  {{ ternary "Always" "IfNotPresent" ( eq (default "" .version) "latest") }}
          env:
          - name: SERVER_NAME
            value: :80
          volumeMounts:
            - mountPath: /var/run/php
              name: php-socket
          ports:
              - name: http
                containerPort: 80
                protocol: TCP
          lifecycle:
            preStop:
              exec:
                command: ["curl", "-XPOST", "http://localhost:2019/stop"]
          livenessProbe:
            httpGet:
              path: /caddy-health-check
              port: http
            periodSeconds: 10
            initialDelaySeconds: 3
          readinessProbe:
            httpGet:
              path: /caddy-health-check
              port: http
          resources:
        {{- toYaml .resources | nindent 12 }}

      volumes:
        - name: config-volume
          configMap:
            name: auth
        - name: jwt
          secret:
            secretName: jwt-secret
        - name: php-socket
          emptyDir: {}
{{- end }}