composer dump-env prod
bin/console lexik:jwt:generate-keypair --overwrite

APISERVER=https://kubernetes.default.svc
SERVICEACCOUNT=/var/run/secrets/kubernetes.io/serviceaccount
NAMESPACE=$(cat ${SERVICEACCOUNT}/namespace)
TOKEN=$(cat ${SERVICEACCOUNT}/token)
CACERT=${SERVICEACCOUNT}/ca.crt

generate_post_data()
{
  cat <<EOF
{
  "apiVersion": "v1",
  "kind": "Secret",
  "metadata": {
    "name": "jwt-secret",
    "namespace": "${NAMESPACE}",
    "annotations": {
      "argocd.argoproj.io/compare-options": "IgnoreExtraneous",
      "argocd.argoproj.io/sync-options": "Prune=false"
    }
  },
  "type": "Opaque",
  "data": {
    "public.pem": "$(cat /srv/api/config/jwt/public.pem | base64)",
    "private.pem": "$(cat /srv/api/config/jwt/private.pem | base64)"
  }
}
EOF
}

http_response=$(curl -s -o response.txt -w "%{http_code}" \
     --cacert ${CACERT} \
     --header "Authorization: Bearer ${TOKEN}" \
     -X PUT "${APISERVER}/api/v1/namespaces/${NAMESPACE}/secrets/jwt-secret" \
     -H "Content-Type: application/json" \
     -d "$(generate_post_data | tr -d '\n')")

if [ $http_response == "404" ]; then
    http_response=$(curl -s -o response.txt -w "%{http_code}" \
            --cacert ${CACERT} \
            --header "Authorization: Bearer ${TOKEN}" \
            -X POST "${APISERVER}/api/v1/namespaces/${NAMESPACE}/secrets" \
            -H "Content-Type: application/json" \
            -d "$(generate_post_data | tr -d '\n')")
fi

if [ $http_response != "200" ]; then
  cat response.txt
  exit 1
fi
